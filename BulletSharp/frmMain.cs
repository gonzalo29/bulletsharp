﻿/*This file is part of BulletSharp
    2014 Max J. Rodríguez Beltran ing.maxjrb[at]gmail.com

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

using System;
using System.Drawing;
using System.Reflection;
using System.Windows.Forms;
using BulletSharp.Properties;

namespace BulletSharp
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
            InitializeComponent();
        }

        private static string _idDev;
        private static bool _acceptCert;
        private static readonly string _version = Assembly.GetExecutingAssembly().GetName().Version.ToString();
        private static readonly string _build = "b070514";

        private void frmMain_Load(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            if (LoadDevices())
            EnablePush();
            Cursor.Current = Cursors.Default;
        }
    
        private bool LoadDevices()
        {
            bool isReloaded = false;

            _acceptCert = Settings.Default.AcceptCerts;

            DisablePush();
            if (Settings.Default.ApiKey.Length <= 0)
            {
                if (DialogResult.OK == MessageBox.Show("API Key not loaded, please set your API Key.", "API Key not loaded",
                    MessageBoxButtons.OKCancel, MessageBoxIcon.Information))
                    using (var frmSet = new frmSettings())
                    {
                        frmSet.ShowDialog();
                    }
                else
                {
                    MessageBox.Show("Devices not loaded, please set your API Key.", "Devices not loaded",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return isReloaded;
                }
            }

            try
            {
                _acceptCert = Settings.Default.AcceptCerts;
                cmbDevices.DataSource = new BindingSource(BulletAPI.BulletAPI.GetDevices(_acceptCert,Settings.Default.ApiKey), null);
                cmbDevices.DisplayMember = "Value";
                cmbDevices.ValueMember = "Key";

                cmbDevices.SelectedItem = cmbDevices.Items[0];
                _idDev = cmbDevices.SelectedValue.ToString();
                isReloaded = true;
            }
            catch (AggregateException ex)
            {
                if (ex.InnerException.Message == "Response status code does not indicate success: 401 (Unauthorized).")
                {
                    MessageBox.Show("Please check your API Key...", "Unauthorized Key.", MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                    LogErrors.LogError(ex.Message + " \r\n" + ex.StackTrace);
                    isReloaded = false;
                }
                else
                {
                    MessageBox.Show("Ups! Something went wrong, please check the log... \r\n"+ex.Message,"Error...",MessageBoxButtons.OK,MessageBoxIcon.Error);
                    LogErrors.LogError(ex.Message + " \r\n" + ex.StackTrace);
                    isReloaded = false;
                }
            }
            catch (BulletAPI.ApiKeyNotFoundException ex)
            {
                MessageBox.Show("API Key not loaded, please set your API Key.", "API Key not loaded",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                LogErrors.LogError(ex.Message + " \r\n" + ex.StackTrace);
                isReloaded = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ups! Something went wrong...");
                var cError = ex.Message + "\r\n" + ex.StackTrace;
                LogErrors.LogError(cError);
                isReloaded = false;
            }
            return isReloaded;
        }

        private void btnPushNote_Click(object sender, EventArgs e)
        {
            BulletAPI.BulletAPI.PushIt(_acceptCert,Settings.Default.ApiKey, "note", _idDev, txtNotes.Text, txtTitleNote.Text);
        }

        private void btnPushLink_Click(object sender, EventArgs e)
        {
            BulletAPI.BulletAPI.PushIt(_acceptCert,Settings.Default.ApiKey, "link", _idDev, txtLink.Text.Trim(), txtTitleLink.Text);
        }

        private void btnPushAddress_Click(object sender, EventArgs e)
        {
            BulletAPI.BulletAPI.PushIt(_acceptCert,Settings.Default.ApiKey, "link", _idDev, txtLink.Text.Trim(), txtTitleAddress.Text);
        }

        private void btnPushList_Click(object sender, EventArgs e)
        {
            var listItems = string.Empty;

            foreach (var item in lbItems.Items)
            {
                listItems += string.Format("&items={0}", item.ToString());
            }

            BulletAPI.BulletAPI.PushIt(_acceptCert,Settings.Default.ApiKey, "list", _idDev, listItems, txtTitleList.Text);
        }

        private void btnFile_Click(object sender, EventArgs e)
        {
            if (openFileDialog.ShowDialog() != DialogResult.OK) return;
            txtFile.Text = openFileDialog.FileName;

            BulletAPI.BulletAPI.PushFile(_acceptCert,Settings.Default.ApiKey, openFileDialog.FileName, _idDev);
        }

        private void tabPage1_Enter(object sender, EventArgs e)
        {
            ChangeSize();
        }

        private void tabPage2_Enter(object sender, EventArgs e)
        {
            //orig size 372; 386 
            this.Size = new Size(372, 223);
        }

        private void tabPage3_Enter(object sender, EventArgs e)
        {
            ChangeSize();
        }

        private void tabPage4_Enter(object sender, EventArgs e)
        {
            ChangeSize();
        }

        private void tabPage5_Enter(object sender, EventArgs e)
        {
            this.Size = new Size(372, 223);
        }

        private void txtItem_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                lbItems.Items.Add(txtItem.Text.Trim());
            }
        }
        
        private void ChangeSize()
        {
            this.Size = new Size(372, 386);
        }

        private void sToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var fSettings = new frmSettings();
            fSettings.ShowDialog();
            Cursor.Current = Cursors.WaitCursor;
            if (LoadDevices())
                EnablePush();
            Cursor.Current = Cursors.Default;
        }
        
        private void mnDeleteItem_Click(object sender, EventArgs e)
        {
            if (lbItems.SelectedIndex != -1)
            lbItems.Items.Remove(lbItems.SelectedItem);
        }
        
        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Bullet Sharp. V"+ _version + _build+" (2014) Released under GPL License. \r\n Developed by Max J. Rodríguez beltrán.\r\n" +
                            "This app use BulletAPI(Library to use Pushbullet from C#)" +
                            "Fork this app: https://bitbucket.org/Jaxmetalmax/bulletsharp", "Bullet Sharp.",
                MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        
        private void reloadDevicesToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            if (LoadDevices())
            {
                MessageBox.Show("Devices Reloaded!");
                Cursor.Current = Cursors.Default;
                EnablePush();
            }
            else
            {
                DisablePush();
            }
        }

        private void cmbDevices_SelectedIndexChanged(object sender, EventArgs e)
        {
            _idDev = cmbDevices.SelectedValue.ToString();
        }

        private void DisablePush()
        {
            if (Settings.Default.ApiKey != "") return;


            foreach (Control btnControl in this.Controls)
            {
                foreach (Control control in this.Controls)
                {
                    EnaButton(control, false);
                }
            }
        }

        private void EnablePush()
        {
            foreach (Control control in this.Controls)
            {
                EnaButton(control,true);
            }
        }

        private void EnaButton(Control con, bool enable)
        {
            if (con is Button)
                con.Enabled = enable;
            else if (con.HasChildren)
            {
                 foreach (Control ctrl in con.Controls)
                {
                    EnaButton(ctrl,enable);
                }
            }   
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}
